const { argv } = require('./config/yargs');
const lugar = require('./lugar/lugar');
const clima = require('./clima/clima');

const getInfo = async(direccion) => {
    try {
        const coords = await lugar.getLugarLatLon(argv.direccion);
        const respClima = await clima.getClima(coords.latitud, coords.longitud);
        return `La temperatura en ${ coords.dir } es ${ respClima } °C`;
    } catch (error) {
        return `No fue posible encontrar el clima de ${ direccion }`
    }
};

getInfo(argv.direccion).then(console.log).catch(console.log);

// lugar.getLugarLatLon(argv.direccion).then(resp => {
//         console.log(`Lugar: ${resp.dir}`);
//         return clima.getClima(resp.latitud, resp.longitud);
//     })
//     .then(resp => {
//         console.log(`Temperatura: ${resp} °C`);
//     });