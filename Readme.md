

## Aplicación de comandos para obtener clima mediante API


Ejercicio del curso
Obtiene la temperatura de una ciudad dada por el usuario mediante la API OpenWeather

En esta sección aprendí a consumir una API por el método GET mediante Axios, el cual es un paquete especial para ello. Axios trabaja con promesas, por lo que es posible trabajar asíncronamente.

Además, me familiaricé con el consumo de una API gracias a la herramienta Postman, pues permite visualizar los parámetros y headers para obtener respuesta de la API.


Instala los paquetes necesarios con el comando:
```
npm install

```


El comando para ejecutar la aplicación es el siguiente: 
```
node app -d {ciudad}

```

Ejemplo:
```
node app -d "New York"

```