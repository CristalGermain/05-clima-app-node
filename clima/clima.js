const axios = require('axios');

const getClima = async(latitud, longitud) => {

    const resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${latitud}&lon=${longitud}&appid=f5f1d212736f4a90329a978cbabe3e08&units=metric`);

    const temp = resp.data.main.temp;

    return temp;
}

module.exports = {
    getClima
}


// mi Key: f5f1d212736f4a90329a978cbabe3e08