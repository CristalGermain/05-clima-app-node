const axios = require('axios');



const getLugarLatLon = async(direccion) => {

    const encodeURL = encodeURI(direccion); //Esta línea arregla una cadena de texto para que sea apta en una URL, por ejemplo, poner '%20' en lugar de espacios
    const instance = axios.create({

        // NUNCA OLVIDES PONER EL HTTPS://
        baseURL: `https://api.openweathermap.org/data/2.5/weather?q=${encodeURL}&appid=f5f1d212736f4a90329a978cbabe3e08&`,
        //Los parámetros aquí son 'q', 'appid'
        // headers: { En esta API no es necesario un header, pero en caso de haber, aquí se integra.}
    });

    const resp = await instance.get();

    const latitud = resp.data.coord.lat;
    const longitud = resp.data.coord.lon;
    const dir = resp.data.name;

    return { latitud, longitud, dir };


}

module.exports = {
    getLugarLatLon
}


// mi Key: f5f1d212736f4a90329a978cbabe3e08